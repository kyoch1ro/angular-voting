import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IVote } from './vote.interface';


@Component({
  selector: 'app-vote',
  templateUrl: './vote.component.html',
  styleUrls: ['./vote.component.css']
})
export class VoteComponent implements OnInit {
  @Input() data: IVote;
  @Output() up: EventEmitter<IVote> = new EventEmitter<IVote>();
  @Output() down: EventEmitter<IVote> = new EventEmitter<IVote>();
  @Output() delete: EventEmitter<IVote> = new EventEmitter<IVote>();

  constructor() { }
  ngOnInit() {
  }

  upVote() {
    this.up.emit(this.data);
  }

  deleteMe() {
    this.delete.emit(this.data);
  }

  downVote() {
    this.down.emit(this.data);
  }

}



