export interface IVote {
    id: number;
    title: string;
    points: number;
}