import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { IVote } from '../vote/vote.interface';


@Component({
  selector: 'app-vote-form',
  templateUrl: './vote-form.component.html',
  styleUrls: ['./vote-form.component.css']
})
export class VoteFormComponent implements OnInit {
  voteForm: FormGroup;
  @Output() submitted: EventEmitter<IVote> = new EventEmitter<IVote>();
  constructor(private fb: FormBuilder) {}

  ngOnInit() {
    this.voteForm = this.fb.group(
      {
        id: [0],
        title: [''],
        points: [0]
      }
    );
  }

  onSubmit() {
    this.submitted.emit(this.voteForm.value);
  }

}
