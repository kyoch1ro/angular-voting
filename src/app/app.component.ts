import { Component } from '@angular/core';
import { IVote } from './vote/vote.interface';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'RON Michael';



  private voting_data: IVote[] = [
    {
      id: 1,
      title: 'Regine',
      points: 0
    },
    {
      id: 2,
      title: 'Pats',
      points: 0
    },
    {
      id: 3,
      title: 'Vince',
      points: 0
    },
    {
      id: 4,
      title: 'Niño',
      points: 0
    }
  ];


  constructor() {

  }

  add(param: any) {
    param.points += 1;
    console.log(param);
  }

  process_vote(vote_type: string, data: IVote) {

    const selected_data =  this.voting_data.find(x => x.id === data.id);


    
    if (vote_type === 'up') {
      selected_data.points += 1;
      this.sortData();
      return;
    }

    selected_data.points -= 1;
    this.sortData();
  }


  sortData() {
    this.voting_data.sort((a, b) => b.points - a.points);
  }


  voteSubmitted(data: IVote) {
    data.id = this.voting_data.length + 1;
    this.voting_data.push(data);
  }

  deleteData(data: IVote) {
    const item_index = this.voting_data.findIndex(x => x.id === data.id);
    this.voting_data.splice(item_index, 1);
  }


}
