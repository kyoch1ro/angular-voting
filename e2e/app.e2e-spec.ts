import { Tut1Page } from './app.po';

describe('tut1 App', () => {
  let page: Tut1Page;

  beforeEach(() => {
    page = new Tut1Page();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
